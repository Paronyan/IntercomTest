package Intercom;

import org.json.JSONObject;
import org.json.JSONException;

public class CustomerOrganiserTest {

    public CustomerOrganiserTest() {}

    public boolean testRead() {

        {
            // incorrect format
            CustomerOrganiser org = new CustomerOrganiser();

            try {
                String line = "{\"lat itude\": \"53.0033946\", \"user_id\": 39, \"name\": \"Lisa Ahearn\", \"longitude\": \"-6.3877505\"}";
                JSONObject obj = org.parseLine(line);
                org.createCustomer(obj);
            } catch (JSONException ex) {
                // do nothing

            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }

        {
            // correct format
            CustomerOrganiser org = new CustomerOrganiser();

            try {
                String line = "{\"latitude\": \"53.0033946\", \"user_id\": 39, \"name\": \"Lisa Ahearn\", \"longitude\": \"-6.3877505\"}";
                JSONObject obj = org.parseLine(line);
                org.createCustomer(obj);
            } catch (JSONException ex) {
                ex.printStackTrace();
                return false;

            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }

        return true;
    }

    public boolean testGetDistance() {

        {
            // correct format
            CustomerOrganiser org = new CustomerOrganiser();

            try {
                String line = "{\"latitude\": \"53.0033946\", \"user_id\": 39, \"name\": \"Lisa Ahearn\", \"longitude\": \"-6.3877505\"}";
                JSONObject obj = org.parseLine(line);
                Customer c = org.createCustomer(obj);

                double distance = org.getDistance(c);

                if (Math.abs(distance - 38.36) > 0.05) {
                    System.err.println("Expecting to get 38.36, got : " + distance);
                    return false;
                }

                line = "{\"latitude\": \"53.339428\", \"user_id\": 39, \"name\": \"Lisa Ahearn\", \"longitude\": \"-6.257664\"}";
                obj = org.parseLine(line);
                c = org.createCustomer(obj);

                distance = org.getDistance(c);

                if (Math.abs(distance) > 0.05) {
                    System.err.println("Expecting to get 38.36, got : " + distance);
                    return false;
                }


            } catch (JSONException ex) {
                ex.printStackTrace();
                return false;

            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }

        }
        return true;
    }
}
