package Intercom;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;

public class Main {

    public static void main(String[] args) {

        String inFile = "/Users/vaheparonyan/Desktop/projects/Intercom/input.txt";
        String outFile = "/Users/vaheparonyan/Desktop/projects/Intercom/output.txt";

        if (args.length == 1 && args[0].equals("TestMe")) {

            testMe();

            return;
        } else if (args.length == 2) {
            inFile = args[1];
            outFile = args[2];
        } else {
            System.out.println("Using default file paths for input and output.");
            System.out.println("please use the following command to pass custom input and output files: ");
            System.out.println("Intercomtest <input file path> <output file path>");
            System.out.println("To run the unit tests please run the following:");
            System.out.println("Intercomtest TestMe");
        }

        readConfig();

        CustomerOrganiser.setConfig(config);
        CustomerOrganiser customerOrganiser = new CustomerOrganiser();

        try {
            customerOrganiser.read(inFile);

            customerOrganiser.dump(outFile);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    private static void readConfig() {
        try {
            try (BufferedReader br = new BufferedReader(new FileReader("../../configuration"))) {

                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                JSONObject c = new JSONObject(sb.toString());
                Iterator it = c.keys();
                while (it.hasNext()) {
                    String key = it.next().toString();
                    config.put(key, c.get(key).toString());
                }
            }
        } catch (Exception ex) {
            System.err.println("Exception happened during parsing the config, default values will be used: ");
            ex.printStackTrace();
        }
    }

    private static void testMe() {
        CustomerOrganiserTest test = new CustomerOrganiserTest();

        boolean ret = test.testRead();
        if (!ret) {
            System.out.println("The test for read method did not pass.");
            System.exit(1);
        }
        System.out.println("The test for read method passed.");
        ret = test.testGetDistance();

        if (!ret) {
            System.out.println("The test for getDistance method did not pass.");
            System.exit(1);
        }
        System.out.println("The test for getDistance method passed.");
    }

    private static HashMap<String, String> config = new HashMap();
}
