package Intercom;

public class Customer {

    public Customer(int user_id, double latitude, double longitude, String name) {
        this.user_id = user_id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public int getUserId() {
        return user_id;
    }
    public String getName() {
        return name;
    }
    // "latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}

    private double latitude;
    private double longitude;
    private int user_id;
    private String name;
}
