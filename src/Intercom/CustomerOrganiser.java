package Intercom;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.TreeMap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class CustomerOrganiser {

    public static void setConfig(HashMap<String, String> config) {
        office[0] = config.get("office.lat") == null ? office[0] : Double.parseDouble(config.get("office.lat"));
        office[1] = config.get("office.long") == null ? office[0] : Double.parseDouble(config.get("office.long"));
        distanceFromOffice = config.get("distanceFromOffice") == null ? office[0] : Double.parseDouble(config.get("distanceFromOffice"));
        EarthRadius = config.get("EarthRadius") == null ? office[0] : Double.parseDouble(config.get("EarthRadius"));
    }

    public CustomerOrganiser() {}

    public void read(String inputFilePath) throws Exception {
        try (BufferedReader br = new BufferedReader(new FileReader(inputFilePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.isEmpty()) {
                    continue;
                }
                JSONObject cust = parseLine(line);
                Customer c = createCustomer(cust);

                if (idToCustomer.containsKey(c.getUserId())) {
                    System.err.println("Error: Parsing line: " + line);
                    throw new Exception("The User with given ID: " + c.getUserId() + ", already exist.");
                }
                idToCustomer.put(c.getUserId(), c);
            }
        }

    }

    public void dump(String outputFilePath) throws Exception {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            for (var o: idToCustomer.entrySet()) {
                Double distance = getDistance(o.getValue());
                if (distanceFromOffice > distance) {
                    System.out.println(o.getKey() + " : " + idToCustomer.get(o.getKey()).getName() + " : " + distance);
                    writer.write(o.getKey() + " : " + idToCustomer.get(o.getKey()).getName() + " : " + distance + "\n");

                }
            }
        }
    }

    double getDistance(Customer c) {
        return getDistance(c.getLatitude(), c.getLongitude(), office[0], office[1]);
    }

    Customer createCustomer(JSONObject cust) throws Exception {
        return new Customer(cust.getInt("user_id"), cust.getDouble("latitude"), cust.getDouble("longitude"), cust.getString("name"));

    }

    JSONObject parseLine(String line) throws Exception {
        JSONObject obj = new JSONObject(line);
        return obj;
    }

    public static double getDistance(double lat1, double lon1,
                                  double lat2, double lon2) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }

        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));


        // calculate the result
        return(c * EarthRadius);
    }

    // id to Dublin office to Customer
    private TreeMap<Integer, Customer> idToCustomer = new TreeMap();

    // these should be read from configuration
    private static double office[] = { 53.339428, -6.257664};
    private static double distanceFromOffice = 100.;

    private static double EarthRadius = 6378.8;

}
