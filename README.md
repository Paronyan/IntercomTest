*Note: my main language is c++, the java is choosen because of the json parser!!*

**Compilation**:

* download the project
* go to Intercom/src/out directory 
* run the following command: `javac -classpath ".:../../java-json.jar:" -d ./ ../Intercom/*.java`


**Run**:
run the following command: `java -cp .:../../java-json.jar  Intercom.Main`
it is expecting to get 2 arguments: input file path and output file path

Ex. `java -cp .:../../java-json.jar  Intercom.Main <input path> <output path>`

**Testing**:
Run same java with 'TestMe' argument: java -cp .:../../java-json.jar  Intercom.Main TestMe

**Design**:
There are 2 main classes and a class for unit testing: `CustomerOrganiserTest`: 

*Intercom.CustomerOrganiser
Intercom.Customer*


Both of them are very intuitive. CustomerOrganiser is responsible for whole logic. It has 2 public methods: `read` and `dump`.

`read` method is parsing the input file and and creating the container of Customer 'objects'. 

We keep all 'Customer' objects in map where we map the Customer.id to Customer object.

`dump` method 
* just iterates over map elements, 
* calculates the distance between Dublin office(read from configuration file) and Customer
* if the distance is smaller than given `distance`(read from configuration file)  
* writes the data into output file, otherwise skips the entry


**Error Handling**:

In case of error during read or dump an exception will be thrown, and main method will
* catch it
* print the message
* exit 

